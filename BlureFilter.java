package org.sample;

import java.awt.image.*;

public class BlureFilter {

    public static final int LINE_MODE = 1;
    public static final int COLUMN_MODE = 2;

    private static final int RAD = 3;
    private static final int RGB = 3;
    private BufferedImage sourceImage;
    private BufferedImage resultImage;
    private int dimX;
    private int dimY;
    private int maxI;
    volatile private int curI;

    public void setImage(BufferedImage image) {
        this.sourceImage = image;
        dimX = image.getWidth();
        dimY = image.getHeight();
        resultImage = new BufferedImage(dimX, dimY, sourceImage.getType());
    }

    public BufferedImage getResult() {
        return resultImage;
    }

    public void pr(int mode, int threadsCount) {
        if (sourceImage != null) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    while (curI < maxI) {
                        prLine(mode, curI++);
                    }
                }
            };
            curI = 0;
            switch (mode) {
                case LINE_MODE:
                    maxI = dimY;
                    break;
                case COLUMN_MODE:
                    maxI = dimX;
                    break;
                default:
                    maxI = 0;
            }

            Thread[] threads = new Thread[threadsCount];
            for (int k = 0; k < threadsCount; k++) {
                threads[k] = new Thread(r);
                threads[k].start();
            }
            for (int k = 0; k < threadsCount; k++) {
                try {
                    threads[k].join();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void prPx(int x, int y) {
        int[] pixelColor = new int[RGB];
        int pixelNumber = 0;
        for (int i = -RAD; i <=RAD; i++) {
            for (int j = -RAD; j <= RAD; j++) {
                if (inImage(x + j, y + i)) {
                    pixelNumber++;
                    int tempColor = sourceImage.getRGB(x + j, y + i);
                    for (int k = 0; k < RGB; k++) {
                        pixelColor[k] += ((tempColor >> (8 * k)) & 0xff);
                    }
                }
            }
        }
        for (int k = 0; k < RGB; k++) {
            pixelColor[k] /= pixelNumber;
        }
        int color = 0;
        for (int k = RGB - 1; k > 0; k--) {
            color |= pixelColor[k];
            color <<= 8;
        }
        color |= pixelColor[0];
        resultImage.setRGB(x, y, color);
    }

    private void prLine(int mode, int index) {
        if (mode == LINE_MODE) {
            for (int j = 0; j < dimX; j++) {
                prPx(j, index);
            }
        }
        else if (mode == COLUMN_MODE) {
            for (int i = 0; i < dimY; i++) {
                prPx(index, i);
            }
        }
    }

    private boolean inImage(int x, int y) {
        return (x >= 0 && y >= 0 && x < dimX && y < dimY);
    }
}
