# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_1_thread_1280x1024

# Run progress: 0,00% complete, ETA 00:56:00
# Fork: 1 of 1
# Warmup Iteration   1: 2,095 s/op
# Warmup Iteration   2: 2,073 s/op
# Warmup Iteration   3: 2,059 s/op
# Warmup Iteration   4: 2,049 s/op
Iteration   1: 2,059 s/op
Iteration   2: 2,048 s/op
Iteration   3: 2,055 s/op
Iteration   4: 2,048 s/op
Iteration   5: 2,052 s/op
Iteration   6: 2,127 s/op
Iteration   7: 2,062 s/op
Iteration   8: 2,047 s/op
Iteration   9: 2,055 s/op
Iteration  10: 2,046 s/op


Result "org.sample.MyBenchmark.Column_1_thread_1280x1024":
  2,060 ±(99.9%) 0,037 s/op [Average]
  (min, avg, max) = (2,046, 2,060, 2,127), stdev = 0,024
  CI (99.9%): [2,023, 2,096] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_1_thread_248x250

# Run progress: 4,17% complete, ETA 00:55:44
# Fork: 1 of 1
# Warmup Iteration   1: 0,099 s/op
# Warmup Iteration   2: 0,096 s/op
# Warmup Iteration   3: 0,096 s/op
# Warmup Iteration   4: 0,096 s/op
Iteration   1: 0,096 s/op
Iteration   2: 0,096 s/op
Iteration   3: 0,096 s/op
Iteration   4: 0,096 s/op
Iteration   5: 0,103 s/op
Iteration   6: 0,099 s/op
Iteration   7: 0,097 s/op
Iteration   8: 0,097 s/op
Iteration   9: 0,098 s/op
Iteration  10: 0,097 s/op


Result "org.sample.MyBenchmark.Column_1_thread_248x250":
  0,097 ±(99.9%) 0,003 s/op [Average]
  (min, avg, max) = (0,096, 0,097, 0,103), stdev = 0,002
  CI (99.9%): [0,094, 0,101] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_1_thread_3840x2160

# Run progress: 8,33% complete, ETA 00:52:41
# Fork: 1 of 1
# Warmup Iteration   1: 13,275 s/op
# Warmup Iteration   2: 13,199 s/op
# Warmup Iteration   3: 13,162 s/op
# Warmup Iteration   4: 13,161 s/op
Iteration   1: 13,327 s/op
Iteration   2: 13,148 s/op
Iteration   3: 13,151 s/op
Iteration   4: 13,253 s/op
Iteration   5: 14,479 s/op
Iteration   6: 13,198 s/op
Iteration   7: 13,436 s/op
Iteration   8: 13,189 s/op
Iteration   9: 13,212 s/op
Iteration  10: 13,171 s/op


Result "org.sample.MyBenchmark.Column_1_thread_3840x2160":
  13,356 ±(99.9%) 0,611 s/op [Average]
  (min, avg, max) = (13,148, 13,356, 14,479), stdev = 0,404
  CI (99.9%): [12,745, 13,968] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_2_threads_1280x1024

# Run progress: 12,50% complete, ETA 00:55:25
# Fork: 1 of 1
# Warmup Iteration   1: 1,291 s/op
# Warmup Iteration   2: 1,293 s/op
# Warmup Iteration   3: 1,265 s/op
# Warmup Iteration   4: 1,253 s/op
Iteration   1: 1,264 s/op
Iteration   2: 1,310 s/op
Iteration   3: 1,277 s/op
Iteration   4: 1,303 s/op
Iteration   5: 1,303 s/op
Iteration   6: 1,271 s/op
Iteration   7: 1,282 s/op
Iteration   8: 1,259 s/op
Iteration   9: 1,277 s/op
Iteration  10: 1,283 s/op


Result "org.sample.MyBenchmark.Column_2_threads_1280x1024":
  1,283 ±(99.9%) 0,026 s/op [Average]
  (min, avg, max) = (1,259, 1,283, 1,310), stdev = 0,017
  CI (99.9%): [1,257, 1,309] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_2_threads_248x250

# Run progress: 16,67% complete, ETA 00:51:38
# Fork: 1 of 1
# Warmup Iteration   1: 0,060 s/op
# Warmup Iteration   2: 0,059 s/op
# Warmup Iteration   3: 0,058 s/op
# Warmup Iteration   4: 0,058 s/op
Iteration   1: 0,058 s/op
Iteration   2: 0,059 s/op
Iteration   3: 0,059 s/op
Iteration   4: 0,060 s/op
Iteration   5: 0,058 s/op
Iteration   6: 0,060 s/op
Iteration   7: 0,058 s/op
Iteration   8: 0,057 s/op
Iteration   9: 0,058 s/op
Iteration  10: 0,059 s/op


Result "org.sample.MyBenchmark.Column_2_threads_248x250":
  0,058 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,057, 0,058, 0,060), stdev = 0,001
  CI (99.9%): [0,057, 0,060] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_2_threads_3840x2160

# Run progress: 20,83% complete, ETA 00:48:12
# Fork: 1 of 1
# Warmup Iteration   1: 8,768 s/op
# Warmup Iteration   2: 8,453 s/op
# Warmup Iteration   3: 8,522 s/op
# Warmup Iteration   4: 8,818 s/op
Iteration   1: 8,999 s/op
Iteration   2: 8,826 s/op
Iteration   3: 8,946 s/op
Iteration   4: 8,342 s/op
Iteration   5: 8,157 s/op
Iteration   6: 8,119 s/op
Iteration   7: 8,239 s/op
Iteration   8: 8,053 s/op
Iteration   9: 8,208 s/op
Iteration  10: 8,519 s/op


Result "org.sample.MyBenchmark.Column_2_threads_3840x2160":
  8,441 ±(99.9%) 0,543 s/op [Average]
  (min, avg, max) = (8,053, 8,441, 8,999), stdev = 0,359
  CI (99.9%): [7,898, 8,984] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_4_threads_1280x1024

# Run progress: 25,00% complete, ETA 00:50:00
# Fork: 1 of 1
# Warmup Iteration   1: 1,066 s/op
# Warmup Iteration   2: 1,085 s/op
# Warmup Iteration   3: 1,082 s/op
# Warmup Iteration   4: 1,080 s/op
Iteration   1: 1,083 s/op
Iteration   2: 1,083 s/op
Iteration   3: 1,084 s/op
Iteration   4: 1,085 s/op
Iteration   5: 1,090 s/op
Iteration   6: 1,086 s/op
Iteration   7: 1,085 s/op
Iteration   8: 1,094 s/op
Iteration   9: 1,086 s/op
Iteration  10: 1,088 s/op


Result "org.sample.MyBenchmark.Column_4_threads_1280x1024":
  1,086 ±(99.9%) 0,005 s/op [Average]
  (min, avg, max) = (1,083, 1,086, 1,094), stdev = 0,004
  CI (99.9%): [1,081, 1,092] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_4_threads_248x250

# Run progress: 29,17% complete, ETA 00:46:39
# Fork: 1 of 1
# Warmup Iteration   1: 0,052 s/op
# Warmup Iteration   2: 0,050 s/op
# Warmup Iteration   3: 0,050 s/op
# Warmup Iteration   4: 0,050 s/op
Iteration   1: 0,050 s/op
Iteration   2: 0,050 s/op
Iteration   3: 0,050 s/op
Iteration   4: 0,050 s/op
Iteration   5: 0,050 s/op
Iteration   6: 0,050 s/op
Iteration   7: 0,050 s/op
Iteration   8: 0,050 s/op
Iteration   9: 0,053 s/op
Iteration  10: 0,051 s/op


Result "org.sample.MyBenchmark.Column_4_threads_248x250":
  0,051 ±(99.9%) 0,002 s/op [Average]
  (min, avg, max) = (0,050, 0,051, 0,053), stdev = 0,001
  CI (99.9%): [0,049, 0,052] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_4_threads_3840x2160

# Run progress: 33,33% complete, ETA 00:43:08
# Fork: 1 of 1
# Warmup Iteration   1: 7,463 s/op
# Warmup Iteration   2: 7,301 s/op
# Warmup Iteration   3: 7,855 s/op
# Warmup Iteration   4: 7,508 s/op
Iteration   1: 7,533 s/op
Iteration   2: 7,491 s/op
Iteration   3: 7,407 s/op
Iteration   4: 7,352 s/op
Iteration   5: 7,283 s/op
Iteration   6: 7,420 s/op
Iteration   7: 7,311 s/op
Iteration   8: 7,480 s/op
Iteration   9: 7,240 s/op
Iteration  10: 7,488 s/op


Result "org.sample.MyBenchmark.Column_4_threads_3840x2160":
  7,400 ±(99.9%) 0,151 s/op [Average]
  (min, avg, max) = (7,240, 7,400, 7,533), stdev = 0,100
  CI (99.9%): [7,249, 7,552] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_8_threads_1280x1024

# Run progress: 37,50% complete, ETA 00:41:45
# Fork: 1 of 1
# Warmup Iteration   1: 1,120 s/op
# Warmup Iteration   2: 1,093 s/op
# Warmup Iteration   3: 1,091 s/op
# Warmup Iteration   4: 1,099 s/op
Iteration   1: 1,093 s/op
Iteration   2: 1,095 s/op
Iteration   3: 1,095 s/op
Iteration   4: 1,096 s/op
Iteration   5: 1,142 s/op
Iteration   6: 1,095 s/op
Iteration   7: 1,096 s/op
Iteration   8: 1,090 s/op
Iteration   9: 1,092 s/op
Iteration  10: 1,089 s/op


Result "org.sample.MyBenchmark.Column_8_threads_1280x1024":
  1,098 ±(99.9%) 0,023 s/op [Average]
  (min, avg, max) = (1,089, 1,098, 1,142), stdev = 0,015
  CI (99.9%): [1,075, 1,122] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_8_threads_248x250

# Run progress: 41,67% complete, ETA 00:38:38
# Fork: 1 of 1
# Warmup Iteration   1: 0,054 s/op
# Warmup Iteration   2: 0,052 s/op
# Warmup Iteration   3: 0,052 s/op
# Warmup Iteration   4: 0,052 s/op
Iteration   1: 0,052 s/op
Iteration   2: 0,052 s/op
Iteration   3: 0,052 s/op
Iteration   4: 0,052 s/op
Iteration   5: 0,052 s/op
Iteration   6: 0,051 s/op
Iteration   7: 0,051 s/op
Iteration   8: 0,051 s/op
Iteration   9: 0,051 s/op
Iteration  10: 0,052 s/op


Result "org.sample.MyBenchmark.Column_8_threads_248x250":
  0,051 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,051, 0,051, 0,052), stdev = 0,001
  CI (99.9%): [0,051, 0,052] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Column_8_threads_3840x2160

# Run progress: 45,83% complete, ETA 00:35:24
# Fork: 1 of 1
# Warmup Iteration   1: 7,236 s/op
# Warmup Iteration   2: 7,159 s/op
# Warmup Iteration   3: 7,250 s/op
# Warmup Iteration   4: 7,152 s/op
Iteration   1: 7,156 s/op
Iteration   2: 7,140 s/op
Iteration   3: 7,224 s/op
Iteration   4: 7,124 s/op
Iteration   5: 7,081 s/op
Iteration   6: 7,043 s/op
Iteration   7: 7,051 s/op
Iteration   8: 7,065 s/op
Iteration   9: 7,067 s/op
Iteration  10: 7,041 s/op


Result "org.sample.MyBenchmark.Column_8_threads_3840x2160":
  7,099 ±(99.9%) 0,091 s/op [Average]
  (min, avg, max) = (7,041, 7,099, 7,224), stdev = 0,060
  CI (99.9%): [7,009, 7,190] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_1_thread_1280x1024

# Run progress: 50,00% complete, ETA 00:33:18
# Fork: 1 of 1
# Warmup Iteration   1: 2,062 s/op
# Warmup Iteration   2: 2,027 s/op
# Warmup Iteration   3: 2,029 s/op
# Warmup Iteration   4: 2,029 s/op
Iteration   1: 2,024 s/op
Iteration   2: 2,060 s/op
Iteration   3: 2,045 s/op
Iteration   4: 2,042 s/op
Iteration   5: 2,026 s/op
Iteration   6: 2,041 s/op
Iteration   7: 2,026 s/op
Iteration   8: 2,030 s/op
Iteration   9: 2,024 s/op
Iteration  10: 2,023 s/op


Result "org.sample.MyBenchmark.Line_1_thread_1280x1024":
  2,034 ±(99.9%) 0,019 s/op [Average]
  (min, avg, max) = (2,023, 2,034, 2,060), stdev = 0,012
  CI (99.9%): [2,015, 2,053] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_1_thread_248x250

# Run progress: 54,17% complete, ETA 00:30:12
# Fork: 1 of 1
# Warmup Iteration   1: 0,103 s/op
# Warmup Iteration   2: 0,102 s/op
# Warmup Iteration   3: 0,102 s/op
# Warmup Iteration   4: 0,102 s/op
Iteration   1: 0,102 s/op
Iteration   2: 0,103 s/op
Iteration   3: 0,102 s/op
Iteration   4: 0,102 s/op
Iteration   5: 0,102 s/op
Iteration   6: 0,102 s/op
Iteration   7: 0,102 s/op
Iteration   8: 0,102 s/op
Iteration   9: 0,102 s/op
Iteration  10: 0,102 s/op


Result "org.sample.MyBenchmark.Line_1_thread_248x250":
  0,102 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,102, 0,102, 0,103), stdev = 0,001
  CI (99.9%): [0,102, 0,103] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_1_thread_3840x2160

# Run progress: 58,33% complete, ETA 00:27:11
# Fork: 1 of 1
# Warmup Iteration   1: 13,279 s/op
# Warmup Iteration   2: 13,168 s/op
# Warmup Iteration   3: 13,145 s/op
# Warmup Iteration   4: 13,142 s/op
Iteration   1: 13,129 s/op
Iteration   2: 13,126 s/op
Iteration   3: 13,137 s/op
Iteration   4: 13,151 s/op
Iteration   5: 13,150 s/op
Iteration   6: 13,212 s/op
Iteration   7: 13,146 s/op
Iteration   8: 13,159 s/op
Iteration   9: 13,158 s/op
Iteration  10: 13,140 s/op


Result "org.sample.MyBenchmark.Line_1_thread_3840x2160":
  13,151 ±(99.9%) 0,036 s/op [Average]
  (min, avg, max) = (13,126, 13,151, 13,212), stdev = 0,024
  CI (99.9%): [13,114, 13,187] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_2_threads_1280x1024

# Run progress: 62,50% complete, ETA 00:24:41
# Fork: 1 of 1
# Warmup Iteration   1: 1,305 s/op
# Warmup Iteration   2: 1,295 s/op
# Warmup Iteration   3: 1,272 s/op
# Warmup Iteration   4: 1,259 s/op
Iteration   1: 1,252 s/op
Iteration   2: 1,268 s/op
Iteration   3: 1,243 s/op
Iteration   4: 1,257 s/op
Iteration   5: 1,284 s/op
Iteration   6: 1,256 s/op
Iteration   7: 1,293 s/op
Iteration   8: 1,280 s/op
Iteration   9: 1,265 s/op
Iteration  10: 1,268 s/op


Result "org.sample.MyBenchmark.Line_2_threads_1280x1024":
  1,266 ±(99.9%) 0,023 s/op [Average]
  (min, avg, max) = (1,243, 1,266, 1,293), stdev = 0,015
  CI (99.9%): [1,243, 1,290] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_2_threads_248x250

# Run progress: 66,67% complete, ETA 00:21:47
# Fork: 1 of 1
# Warmup Iteration   1: 0,059 s/op
# Warmup Iteration   2: 0,057 s/op
# Warmup Iteration   3: 0,058 s/op
# Warmup Iteration   4: 0,057 s/op
Iteration   1: 0,057 s/op
Iteration   2: 0,058 s/op
Iteration   3: 0,058 s/op
Iteration   4: 0,058 s/op
Iteration   5: 0,059 s/op
Iteration   6: 0,059 s/op
Iteration   7: 0,059 s/op
Iteration   8: 0,059 s/op
Iteration   9: 0,058 s/op
Iteration  10: 0,058 s/op


Result "org.sample.MyBenchmark.Line_2_threads_248x250":
  0,058 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,057, 0,058, 0,059), stdev = 0,001
  CI (99.9%): [0,057, 0,059] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_2_threads_3840x2160

# Run progress: 70,83% complete, ETA 00:18:54
# Fork: 1 of 1
# Warmup Iteration   1: 7,808 s/op
# Warmup Iteration   2: 7,908 s/op
# Warmup Iteration   3: 8,123 s/op
# Warmup Iteration   4: 7,750 s/op
Iteration   1: 8,059 s/op
Iteration   2: 7,712 s/op
Iteration   3: 7,705 s/op
Iteration   4: 7,843 s/op
Iteration   5: 7,703 s/op
Iteration   6: 7,790 s/op
Iteration   7: 7,911 s/op
Iteration   8: 7,902 s/op
Iteration   9: 7,732 s/op
Iteration  10: 7,757 s/op


Result "org.sample.MyBenchmark.Line_2_threads_3840x2160":
  7,811 ±(99.9%) 0,177 s/op [Average]
  (min, avg, max) = (7,703, 7,811, 8,059), stdev = 0,117
  CI (99.9%): [7,635, 7,988] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_4_threads_1280x1024

# Run progress: 75,00% complete, ETA 00:16:32
# Fork: 1 of 1
# Warmup Iteration   1: 1,052 s/op
# Warmup Iteration   2: 1,062 s/op
# Warmup Iteration   3: 1,068 s/op
# Warmup Iteration   4: 1,065 s/op
Iteration   1: 1,067 s/op
Iteration   2: 1,066 s/op
Iteration   3: 1,078 s/op
Iteration   4: 1,090 s/op
Iteration   5: 1,093 s/op
Iteration   6: 1,095 s/op
Iteration   7: 1,119 s/op
Iteration   8: 1,092 s/op
Iteration   9: 1,068 s/op
Iteration  10: 1,067 s/op


Result "org.sample.MyBenchmark.Line_4_threads_1280x1024":
  1,083 ±(99.9%) 0,026 s/op [Average]
  (min, avg, max) = (1,066, 1,083, 1,119), stdev = 0,017
  CI (99.9%): [1,057, 1,110] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_4_threads_248x250

# Run progress: 79,17% complete, ETA 00:13:42
# Fork: 1 of 1
# Warmup Iteration   1: 0,051 s/op
# Warmup Iteration   2: 0,051 s/op
# Warmup Iteration   3: 0,051 s/op
# Warmup Iteration   4: 0,051 s/op
Iteration   1: 0,051 s/op
Iteration   2: 0,051 s/op
Iteration   3: 0,051 s/op
Iteration   4: 0,051 s/op
Iteration   5: 0,051 s/op
Iteration   6: 0,051 s/op
Iteration   7: 0,051 s/op
Iteration   8: 0,051 s/op
Iteration   9: 0,051 s/op
Iteration  10: 0,051 s/op


Result "org.sample.MyBenchmark.Line_4_threads_248x250":
  0,051 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,051, 0,051, 0,051), stdev = 0,001
  CI (99.9%): [0,051, 0,051] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_4_threads_3840x2160

# Run progress: 83,33% complete, ETA 00:10:53
# Fork: 1 of 1
# Warmup Iteration   1: 6,740 s/op
# Warmup Iteration   2: 6,747 s/op
# Warmup Iteration   3: 6,759 s/op
# Warmup Iteration   4: 6,749 s/op
Iteration   1: 6,763 s/op
Iteration   2: 6,759 s/op
Iteration   3: 6,766 s/op
Iteration   4: 6,760 s/op
Iteration   5: 6,982 s/op
Iteration   6: 6,937 s/op
Iteration   7: 6,792 s/op
Iteration   8: 6,779 s/op
Iteration   9: 6,762 s/op
Iteration  10: 6,754 s/op


Result "org.sample.MyBenchmark.Line_4_threads_3840x2160":
  6,805 ±(99.9%) 0,125 s/op [Average]
  (min, avg, max) = (6,754, 6,805, 6,982), stdev = 0,083
  CI (99.9%): [6,680, 6,930] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_8_threads_1280x1024

# Run progress: 87,50% complete, ETA 00:08:14
# Fork: 1 of 1
# Warmup Iteration   1: 1,097 s/op
# Warmup Iteration   2: 1,088 s/op
# Warmup Iteration   3: 1,097 s/op
# Warmup Iteration   4: 1,129 s/op
Iteration   1: 1,092 s/op
Iteration   2: 1,086 s/op
Iteration   3: 1,127 s/op
Iteration   4: 1,200 s/op
Iteration   5: 1,126 s/op
Iteration   6: 1,095 s/op
Iteration   7: 1,081 s/op
Iteration   8: 1,084 s/op
Iteration   9: 1,101 s/op
Iteration  10: 1,098 s/op


Result "org.sample.MyBenchmark.Line_8_threads_1280x1024":
  1,109 ±(99.9%) 0,054 s/op [Average]
  (min, avg, max) = (1,081, 1,109, 1,200), stdev = 0,036
  CI (99.9%): [1,055, 1,163] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_8_threads_248x250

# Run progress: 91,67% complete, ETA 00:05:28
# Fork: 1 of 1
# Warmup Iteration   1: 0,052 s/op
# Warmup Iteration   2: 0,052 s/op
# Warmup Iteration   3: 0,052 s/op
# Warmup Iteration   4: 0,052 s/op
Iteration   1: 0,053 s/op
Iteration   2: 0,053 s/op
Iteration   3: 0,053 s/op
Iteration   4: 0,053 s/op
Iteration   5: 0,054 s/op
Iteration   6: 0,052 s/op
Iteration   7: 0,051 s/op
Iteration   8: 0,051 s/op
Iteration   9: 0,051 s/op
Iteration  10: 0,052 s/op


Result "org.sample.MyBenchmark.Line_8_threads_248x250":
  0,052 ±(99.9%) 0,001 s/op [Average]
  (min, avg, max) = (0,051, 0,052, 0,054), stdev = 0,001
  CI (99.9%): [0,051, 0,054] (assumes normal distribution)


# JMH version: 1.21
# VM version: JDK 10.0.2, Java HotSpot(TM) 64-Bit Server VM, 10.0.2+13
# VM invoker: C:\Program Files\Java\jdk-10.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1251
# Warmup: 4 iterations, 10 s each
# Measurement: 10 iterations, 10 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: org.sample.MyBenchmark.Line_8_threads_3840x2160

# Run progress: 95,83% complete, ETA 00:02:43
# Fork: 1 of 1
# Warmup Iteration   1: 6,788 s/op
# Warmup Iteration   2: 6,774 s/op
# Warmup Iteration   3: 6,756 s/op
# Warmup Iteration   4: 6,798 s/op
Iteration   1: 6,774 s/op
Iteration   2: 6,796 s/op
Iteration   3: 7,276 s/op
Iteration   4: 7,003 s/op
Iteration   5: 6,806 s/op
Iteration   6: 6,796 s/op
Iteration   7: 6,790 s/op
Iteration   8: 6,807 s/op
Iteration   9: 6,770 s/op
Iteration  10: 6,777 s/op


Result "org.sample.MyBenchmark.Line_8_threads_3840x2160":
  6,860 ±(99.9%) 0,244 s/op [Average]
  (min, avg, max) = (6,770, 6,860, 7,276), stdev = 0,162
  CI (99.9%): [6,615, 7,104] (assumes normal distribution)


# Run complete. Total time: 01:05:44

Benchmark                               Mode  Cnt   Score    Error  Units
MyBenchmark.Column_1_thread_1280x1024   avgt   10   2,060 ±  0,037   s/op
MyBenchmark.Column_1_thread_248x250     avgt   10   0,097 ±  0,003   s/op
MyBenchmark.Column_1_thread_3840x2160   avgt   10  13,356 ±  0,611   s/op
MyBenchmark.Column_2_threads_1280x1024  avgt   10   1,283 ±  0,026   s/op
MyBenchmark.Column_2_threads_248x250    avgt   10   0,058 ±  0,001   s/op
MyBenchmark.Column_2_threads_3840x2160  avgt   10   8,441 ±  0,043   s/op
MyBenchmark.Column_4_threads_1280x1024  avgt   10   1,086 ±  0,005   s/op
MyBenchmark.Column_4_threads_248x250    avgt   10   0,051 ±  0,002   s/op
MyBenchmark.Column_4_threads_3840x2160  avgt   10   7,400 ±  0,151   s/op
MyBenchmark.Column_8_threads_1280x1024  avgt   10   1,098 ±  0,023   s/op
MyBenchmark.Column_8_threads_248x250    avgt   10   0,051 ±  0,001   s/op
MyBenchmark.Column_8_threads_3840x2160  avgt   10   7,099 ±  0,091   s/op
MyBenchmark.Line_1_thread_1280x1024     avgt   10   2,034 ±  0,019   s/op
MyBenchmark.Line_1_thread_248x250       avgt   10   0,102 ±  0,001   s/op
MyBenchmark.Line_1_thread_3840x2160     avgt   10  13,151 ±  0,036   s/op
MyBenchmark.Line_2_threads_1280x1024    avgt   10   1,266 ±  0,023   s/op
MyBenchmark.Line_2_threads_248x250      avgt   10   0,058 ±  0,001   s/op
MyBenchmark.Line_2_threads_3840x2160    avgt   10   7,811 ±  0,177   s/op
MyBenchmark.Line_4_threads_1280x1024    avgt   10   1,083 ±  0,026   s/op
MyBenchmark.Line_4_threads_248x250      avgt   10   0,051 ±  0,001   s/op
MyBenchmark.Line_4_threads_3840x2160    avgt   10   6,805 ±  0,125   s/op
MyBenchmark.Line_8_threads_1280x1024    avgt   10   1,109 ±  0,054   s/op
MyBenchmark.Line_8_threads_248x250      avgt   10   0,052 ±  0,001   s/op
MyBenchmark.Line_8_threads_3840x2160    avgt   10   6,860 ±  0,244   s/op

Process finished with exit code 0
