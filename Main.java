package org.sample;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class Main {
    public static void filterSetup(String dirName, String fileName, BlureFilter filter) {
        try {
            File pic = new File(dirName + fileName);
            FileInputStream fis = new FileInputStream(pic);
            filter.setImage(ImageIO.read(fis));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        String dirName = "C:\\Users\\Дима\\HW_2\\pictrures\\";
        String fileName = "spaceship_backgrounds.jpg";
        BlureFilter filter = new BlureFilter();

        filterSetup(dirName, fileName, filter);
        filter.pr(BlureFilter.LINE_MODE, 4);

        BufferedImage resultImage = filter.getResult();

        try {
            ImageIO.write(resultImage, "jpg", new File(dirName, "Blured - " + fileName));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}