package org.sample;

import org.openjdk.jmh.annotations.*;
import java.util.concurrent.TimeUnit;


public class MyBenchmark {
    @State(Scope.Thread)
    public static class State1 {
        @Setup(Level.Trial)
        public void doSetup() {
            Main.filterSetup("C:\\Users\\Дима\\HW_2\\pictrures\\",
                    "bodybuilder.jpg", filter);
        }
        public BlureFilter filter = new BlureFilter();
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_1_thread_248x250(State1 state) {
        state.filter.pr(state.filter.LINE_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_2_threads_248x250(State1 state) {
        state.filter.pr(state.filter.LINE_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_4_threads_248x250(State1 state) {
        state.filter.pr(state.filter.LINE_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_8_threads_248x250(State1 state) {
        state.filter.pr(state.filter.LINE_MODE, 8);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_1_thread_248x250(State1 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_2_threads_248x250(State1 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_4_threads_248x250(State1 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_8_threads_248x250(State1 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 8);
    }
    @State(Scope.Thread)
    public static class State2 {
        @Setup(Level.Trial)
        public void doSetup() {
            Main.filterSetup("C:\\Users\\Дима\\HW_2\\pictrures\\",
                    "starcraft_cat.jpg", filter);
        }
        public BlureFilter filter = new BlureFilter();
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_1_thread_1280x1024(State2 state) {
        state.filter.pr(state.filter.LINE_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_2_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.LINE_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_4_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.LINE_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_8_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.LINE_MODE, 8);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_1_thread_1280x1024(State2 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_2_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_4_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_8_threads_1280x1024(State2 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 8);
    }
    @State(Scope.Thread)
    public static class State3 {
        @Setup(Level.Trial)
        public void doSetup() {
            Main.filterSetup("C:\\Users\\Дима\\HW_2\\pictrures\\",
                    "alien_covenant_4k.jpg", filter);
        }
        public BlureFilter filter = new BlureFilter();
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_1_thread_3840x2160(State3 state) {
        state.filter.pr(state.filter.LINE_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_2_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.LINE_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_4_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.LINE_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Line_8_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.LINE_MODE, 8);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_1_thread_3840x2160(State3 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 1);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_2_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 2);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_4_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 4);
    }
    @Benchmark @Fork(1) @Warmup(iterations = 4) @Measurement(iterations = 10) @BenchmarkMode(Mode.AverageTime) @OutputTimeUnit(TimeUnit.SECONDS)
    public void Column_8_threads_3840x2160(State3 state) {
        state.filter.pr(state.filter.COLUMN_MODE, 8);
    }
}
